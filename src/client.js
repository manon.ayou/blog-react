import sanityClient from "@sanity/client";

export default sanityClient({
    projectId: "mvzw723d",
    dataset: "production",
    useCdn: true,
})