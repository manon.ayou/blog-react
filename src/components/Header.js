import React from "react";
import NavBar from "./NavBar";

const Header = () => {
	return (
		<header>
			<div className="bg-white-300">
				<NavBar />
			</div>
		</header>
	);
};

export default Header;