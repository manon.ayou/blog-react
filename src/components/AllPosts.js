// src/components/AllPosts.js

import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import sanityClient from "../client.js";


export default function AllPosts() {
  const [allPostsData, setAllPosts] = useState(null);

  useEffect(() => {
    sanityClient
      .fetch(
        `*[_type == "post"]{
        title,
        slug,
        category,
        mainImage{
          asset->{
          _id,
          url
        }
      }
    }`
      )
      .then((data) => setAllPosts(data))
      .catch(console.error);
  }, []);

  return (
    <div className="bg-white-100 min-h-screen p-24">
      <div className="container mx-auto">
        <h2 className="text-5xl text-gray-400 flex justify-center cursive">A simple blog</h2>
        <h3 id="blog_intro" className="text-2xl text-gray-600 flex justify-center mb-24">
          Welcome to my blog posts page!
        </h3>
        <div className="grid md:grid-cols-2 xl:grid-cols-3 gap-8">
          {allPostsData &&
            allPostsData.map((post, index) => (
              <Link to={"/" + post.slug.current} key={post.slug.current}>
                <span
                  className="block h-64 relative rounded shadow leading-snug bg-white
                      border-b-8 border-red-800 mx-2"
                  key={index}
                >
                  <img
                    className="w-full h-full rounded-t object-cover absolute"
                    src={post.mainImage.asset.url}
                    alt=""
                  />
                  {/*<span
                    className="block relative h-full flex justify-end items-end pr
                      -4 pb-4"
                  >
                    <h2
                      className="text-gray-700 text-lg font-bold px-3 py-4 bg-red-800
                        text-red-100 bg-opacity-85 rounded"
                    >
                      {post.title}
                    </h2>
                  </span>*/}
                </span>
                <span class="text-xl hover:underline">
                  {post.title}
                </span>
              </Link>
            ))}
        </div>
      </div>
    </div>
  );
}