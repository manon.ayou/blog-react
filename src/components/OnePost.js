import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import sanityClient from "../client.js";
import BlockContent from "@sanity/block-content-to-react";
import imageUrlBuilder from "@sanity/image-url";



const builder = imageUrlBuilder(sanityClient);
function urlFor(source) {
    return builder.image(source);
}


export default function OnePost() {
    const [OnePostData, setPostData] = useState(null);
    const { slug } = useParams();

    useEffect(() => {
        sanityClient
            .fetch(
                `*[slug.current == $slug] {
                    title,
                    slug,
                    category,
                    mainImage{
                        asset->{
                            _id,
                            url
                        }
                    },
                 body,
                 "name": author->name,
                 "authorImage": author->image    
                }`,
                    { slug }
            )
            .then((data) =>setPostData(data[0]))
            .catch(console.error);
    }, [slug] );


    if(!OnePostData) return <div>Loading...</div>;

    return(
        <div class="container mx-auto">
            <div class="flex justify-center mt-24 mb-16">
            <img src={urlFor(OnePostData.mainImage).width(600).url()} alt="" />
            </div>
            <div class="text-5xl font-bold mb-24">
                <h2> {OnePostData.title} </h2>   
            </div>
            <div> {OnePostData.categories} </div>
            <div class="text-justify text-lg">
                <BlockContent
                    blocks={OnePostData.body}
                    projectId={sanityClient.clientConfig.projectId}
                    dataset={sanityClient.clientConfig.dataset}
                />
            </div>
            <div class="flex items-center justify-end space-x-8 mt-16 font-semibold ">
                    <div class="text-xl">
                        <h4>{OnePostData.name}</h4>
                    </div>
                    <img 
                        src={urlFor(OnePostData.authorImage).width(70).url()}
                        alt="Manon AYOU"
                    />
            </div>
        </div>
    );
}