import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import AllPosts from "./components/AllPosts.js";
import OnePost from "./components/OnePost.js";
import Header from "./components/Header.js";
import About from "./components/About.js";


function App() {
  return (
    <BrowserRouter>
				<Header />
        <Switch>
          <Route component={AllPosts} path="/" exact />
          <Route component={OnePost} path="/:slug" />
          <Route component={About} path="/about" exact/>
        </Switch>
    </BrowserRouter>
  );
}
export default App;